(function () {
'use strict';

angular.module('app.feed', ['app.core'])
.config(['$stateProvider', '$urlRouterProvider', config])

function config($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app.feed', {
      url: '/feed',
      views: {
        'content': {
          controller: 'FeedController',
          templateUrl: 'templates/feed.html'
        }
      }
    });

    $urlRouterProvider
      .when('/', '/feed')
      .otherwise('/feed');
}

})();

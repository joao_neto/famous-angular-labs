(function () {
'use strict';

angular.module('app', [
  'app.core',
  'app.sidemenu',
  'app.connections',
  'app.feed',
])
.controller('AppController', ['$scope', '$sidemenu', '$page', AppController])
.config(['$stateProvider', '$urlRouterProvider', configApp])
.run(['$famous', runApp])

function AppController($scope, $sidemenu, $page) {
  $scope.$sidemenu = $sidemenu;
  $scope.$page = $page;

  $page.statusBarMargin = 20;

  $page.scroll.pipe($sidemenu.swipe);

  $scope.headerOptions = {
    classes: ['header'],
    size: [undefined, 44],
    properties: {
      lineHeight: '44px'
    }
  };
}

function configApp($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('app', {
      abstract: true,
      views: {
        'app': {
          controller: 'AppController',
          templateUrl: 'templates/layout.default.html'
        },
        'sidemenu@app': {
          controller: 'SideMenuController',
          templateUrl: 'templates/sidemenu.html'
        },
        'header@app': {
          templateUrl: 'templates/header.html'
        },
      }
    });

    $urlRouterProvider
      .when('', '/feed')
      .when('/', '/feed')
      .otherwise('/feed');
}

function runApp($famous) {
  var FastClick = $famous['famous/inputs/FastClick'];
}

})();

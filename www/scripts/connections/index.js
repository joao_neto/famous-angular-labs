(function () {
'use strict';

angular.module('app.connections', ['app.core'])
.config(['$stateProvider', config])

function config($stateProvider) {
  $stateProvider
    .state('app.connections', {
      url: '/connections',
      views: {
        'content': {
          controller: 'ConnectionsController',
          templateUrl: 'templates/connections.html'
        }
      }
    });
}
})();

(function () {
'use strict';

angular.module('app.connections')
.controller('ConnectionsController', ['$scope', '$page', ConnectionsController])

function ConnectionsController($scope, $page) {
  $page.title = 'Conexões';
}

})();

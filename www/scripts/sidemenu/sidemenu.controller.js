(function () {
'use strict';

angular.module('app.sidemenu')
.controller('SideMenuController', ['$scope', SideMenuController])

function SideMenuController($scope) {
  $scope.items = [
    {
      title: 'Feed',
      uiSref: '.feed',
      options: {
        classes: ['item']
      }
    },
    {
      title: 'Conexões',
      uiSref: '.connections',
      options: {
        classes: ['item']
      }
    }
  ];
}

})();

(function () {
'use strict';

angular.module('app.sidemenu')
.service('$sidemenu', ['$rootScope', '$famous', sidemenuService])

function sidemenuService($rootScope, $famous) {
  var Engine = $famous['famous/core/Engine'];
  var EventHandler = $famous['famous/core/EventHandler'];
  var GenericSync = $famous['famous/inputs/GenericSync'];
  var MouseSync = $famous['famous/inputs/MouseSync'];
  var TouchSync = $famous['famous/inputs/TouchSync'];
  var Transform = $famous['famous/core/Transform'];
  var Transitionable = $famous['famous/transitions/Transitionable'];

  var startPos = 0;
  var currentPos = 0;

  var sideMenuService = {
    isOpen: false,
    size: 276,
    clientSwipe: false,
    eventHandler: new EventHandler(),
    hide: function () {
      this.opacity.set(0, { duration: 300 });
      this.transform.set(0, { duration: 300, curve: 'easeOut' }, function () {
        this.currentPos = 0;
        this.isOpen = false;
      }.bind(this));
      return this;
    },
    show: function () {
      this.opacity.set(1, { duration: 300 });
      this.transform.set(100, { duration: 300, curve: 'easeOut' }, function () {
        this.currentPos = 100;
        this.isOpen = true;
      }.bind(this));
      return this;
    },
    toggleMenu: function () {
      if (this.isOpen) {
        this.hide();
      } else {
        this.show();
      }
      return this;
    }
  };

  sideMenuService.transform = new Transitionable(0);
  sideMenuService.opacity = new Transitionable(0);

  GenericSync.register({ 'mouse': MouseSync, 'touch': TouchSync });

  // menu item click
  sideMenuService.eventHandler.on('click', function (e) {
    if (this.isOpen) this.hide();
  }.bind(sideMenuService));

  sideMenuService.swipe = new GenericSync(['mouse', 'touch'], { direction: GenericSync.DIRECTION_X });
  sideMenuService.swipe.on('start', function (data) {
    startPos = data.clientX;
  }.bind(sideMenuService));

  sideMenuService.swipe.on('update', function (data) {
    currentPos = Math.min(100, this.transform.get() + data.delta / (this.size / 100));
    if (startPos < 20 || this.isOpen) {
      this.opacity.set(currentPos*.01);
      this.transform.set(currentPos);
    }
  }.bind(sideMenuService));

  sideMenuService.swipe.on('end', function (data) {
    var velocity = data.velocity;

    if (this.clientSwipe && startPos > 20 && (velocity > 0.70 || velocity < -0.70)) {
      if (velocity > 0.70) {
        this.show();
      } else {
        this.hide();
      }
    } else {
      if (velocity > 0 && currentPos > 25) {
        this.show();
      } else {
        this.hide();
      }
    }

    startPos = 0;
  }.bind(sideMenuService));

  return sideMenuService;
}

})();

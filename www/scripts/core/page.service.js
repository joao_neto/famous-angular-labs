(function () {
'use strict';

angular.module('app.core')
.service('$page', ['$rootScope', '$famous', pageService])

function pageService($rootScope, $famous) {
  var Engine = $famous['famous/core/Engine'];
  var Transitionable = $famous['famous/transitions/Transitionable'];
  var Easing = $famous['famous/transitions/Easing'];
  var EventHandler = $famous['famous/core/EventHandler'];

  var pageService = {
    width: window.innerWidth,
    height: window.innerWidth,
    transform: new Transitionable(0),
    opacity: new Transitionable(1),
    scroll: new EventHandler(),
    statusBarMargin: 0
  };

  Engine.on('resize', function () {
    this.width = window.innerWidth;
    this.height = window.innerHeight;
  }.bind(pageService));

  // $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
  //   this.opacity.set(1);
  //   this.transform.set(0, { duration: 600, curve: 'easeOut' });
  // }.bind(pageService));

  // $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
  //   this.opacity.set(0);
  //   this.transform.set(this.width*2);
  // }.bind(pageService));

  return pageService;
}

})();

(function() {
'use strict';

angular
.module('app.core')
.directive('expAvatarImg', [expAvatarImg]);

function expAvatarImg() {
  var directive = {
    restrict: 'E',
    link: link
  };

  return directive;

  function link (scope, element, attrs) {
    attrs.$observe('avatarSrc', function (val) {
      if (attrs.avatarSrc) {
        var img = angular.element('<img src="'+attrs.avatarSrc+'">');
      }
      element.empty().append(img);
    });

    attrs.$observe('text', function (val) {
      if (attrs.avatarSrc) { return; }
      var letter = val[0] || '';
      var svg = angular.element('<svg viewBox="0 0 58 58" width="100%" height="100%"><g id="layer1"><path id="path2987" fill="#CCC" d="M54.074449,14.430442,54.154832,43.43033,29.080383,57.999889,3.9255514,43.569558,3.8451682,14.56967,28.919617,0.00011140474z"/><text id="text3757" style="letter-spacing:0px;word-spacing:0px;" font-weight="normal" xml:space="preserve" font-size="40px" font-style="normal" y="43.481136" x="15.328125" font-family="monospace" line-height="125%" fill="#034282"><tspan id="text" x="16.328125" y="43.481136">' + letter + '</tspan></text></g></svg>');
      element.empty().append(svg);
    }, true);
  }

}

})();

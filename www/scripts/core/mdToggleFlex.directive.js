(function() {
'use strict';

angular
.module('app.core')
.directive('mdToggleFlex', [mdToggleFlex]);

function mdToggleFlex () {
  var directive = {
    restrict: 'A',
    link: link
  };

  return directive;

  function link (scope, element, attrs) {
    scope.$watch(attrs.mdToggleFlex, function (val) {
      if (val) {
        element.attr('flex', '');
      } else {
        element.removeAttr('flex');
      }
    });
  }

}

})();

(function() {
'use strict';

angular
.module('app.core')
.directive('mdSvgIcon', ['$http', mdSvgIcon]);

function mdSvgIcon ($http) {
   var directive = {
    restrict: 'E',
    template: '<div class="md-icon"></div>',
    link: link
  };

  return directive;

  function link (scope, element, attrs) {
    attrs.$observe('href', function (val) {
      $http.get(val, { cache: true })
        .then(function (res) {
          var $svg = angular.element(res.data);
          $svg.css('fill', attrs.fill || 'currentColor');
          element.empty().append($svg);
        });
    });

    attrs.$observe('icon', function (val) {
      if (attrs.href) { return; }
      var $svg = angular.element('<svg><use xlink:href="#'+val+'"></use></svg>');
      $svg.attr('viewBox', '0 0 24 24');
      $svg.attr('class', 'icon');
      element.empty().append($svg);
    });
  }

}

})();
